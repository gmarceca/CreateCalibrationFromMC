#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <CreateCalibrationTreeFromxAOD/xAODAnalysis.h>
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "xAODTruth/TruthEventContainer.h"

//JetCalibrationTool
#include "JetCalibTools/JetCalibrationTool.h"

#include "TF1.h"

#include <iostream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(xAODAnalysis)

    //void c1::set_status(int state)
    //{
    //  status = state;
    //cout<<status<<endl;
    //}

xAODAnalysis :: xAODAnalysis ()
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
}


xAODAnalysis :: xAODAnalysis (string config)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().

    m_config = config;

    m_isCollisions = false;
    JES_config_file = "JES_MC15Prerecommendation_April2015.config";
}



EL::StatusCode xAODAnalysis :: setupJob (EL::Job& job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.

    job.useXAOD ();

    // let's initialize the algorithm to use the xAODRootAccess package
    xAOD::Init( "xAODAnalysis" ).ignore(); // call before opening first file

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: histInitialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    h_events = new TH1F("h_events","total events", 10, 0, 10); 
    h_ev_LC_cutFlow = new TH1F("h_ev_LC_cutFlow","event LC cutFlow", 10, 0, 10); 
    h_ev_EM_cutFlow = new TH1F("h_ev_EM_cutFlow","event EM cutFlow", 10, 0, 10); 
    h_ev_10_cutFlow = new TH1F("h_ev_10_cutFlow","event 1.0 jets cutFlow", 10, 0, 10); 
    h_jet_EM_cutFlow = new TH1F("h_jet_EM_cutFlow","jet EM cutFlow", 10, 0, 10); 
    h_jet_LC_cutFlow = new TH1F("h_jet_LC_cutFlow","jet LC cutFlow", 10, 0, 10); 
    h_10jet_cutFlow = new TH1F("h_10jet_cutFlow","1.0 jet cutFlow", 10, 0, 10); 
    h_jet_T_LC_cutFlow = new TH1F("h_jet_T_LC_cutFlow","truth jet cutFlow for LC jets", 10, 0, 10); 
    h_jet_T_EM_cutFlow = new TH1F("h_jet_T_EM_cutFlow","truth jet cutFlow for EM jets", 10, 0, 10); 
    h_10jet_T_cutFlow = new TH1F("h_10jet_T_cutFlow","truth jet cutFlow for 1.0 jets", 10, 0, 10); 

    h_dr_10Matching = new TH1F("h_dr_10Matching","closest dr 10 jets", 200, 0, 2); 
    h_matching_ineff_count = new TH1F("h_matching_ineff_count","matching reco-truth inefficiency counts", 10, 0, 10); 
    h_totalTjets_count = new TH1F("h_totalTjets_count","total truth jets counts", 10, 0, 10); 

    h_dr_2d_10Matching = new TH2F("h_dr_2d_10Matching","closest dr 10 jets vs pT", 100,0,300,200, 0, 2); 

    h_dr_10Matching->GetXaxis()->SetTitle("closest dr 1.0 reco-truth jets");
    h_dr_2d_10Matching->GetYaxis()->SetTitle("closest dr 1.0 reco-truth jets");
    h_dr_2d_10Matching->GetXaxis()->SetTitle("reco jet pT [GeV]");

    h_ev_LC_cutFlow->GetXaxis()->SetTitle("Event cutFlow for LC jets");
    h_ev_LC_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_ev_LC_cutFlow->GetXaxis()->SetBinLabel(2,"PV PU cut");
    h_ev_LC_cutFlow->GetXaxis()->SetBinLabel(3,"Avg pT cut");

    h_ev_EM_cutFlow->GetXaxis()->SetTitle("Event cutFlow for EM jets");
    h_ev_EM_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_ev_EM_cutFlow->GetXaxis()->SetBinLabel(2,"PV PU cut");
    h_ev_EM_cutFlow->GetXaxis()->SetBinLabel(3,"Avg pT cut");

    h_ev_10_cutFlow->GetXaxis()->SetTitle("Event cutFlow for 1.0 jets");
    h_ev_10_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_ev_10_cutFlow->GetXaxis()->SetBinLabel(2,"PV PU cut");
    h_ev_10_cutFlow->GetXaxis()->SetBinLabel(3,"Avg pT cut");

    h_jet_EM_cutFlow->GetXaxis()->SetTitle("Jet cutFlow for EM jets");
    h_jet_EM_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_jet_EM_cutFlow->GetXaxis()->SetBinLabel(2,"Unphysical jet");
    h_jet_EM_cutFlow->GetXaxis()->SetBinLabel(3,"Cleaning tool");

    h_jet_LC_cutFlow->GetXaxis()->SetTitle("Jet cutFlow for LC jets");
    h_jet_LC_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_jet_LC_cutFlow->GetXaxis()->SetBinLabel(2,"Unphysical jet");
    h_jet_LC_cutFlow->GetXaxis()->SetBinLabel(3,"Cleaning tool");

    h_10jet_cutFlow->GetXaxis()->SetTitle("Jet cutFlow for 1.0 jets");
    h_10jet_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_10jet_cutFlow->GetXaxis()->SetBinLabel(2,"Unphysical jet");
    h_10jet_cutFlow->GetXaxis()->SetBinLabel(3,"Cleaning tool");

    h_jet_T_EM_cutFlow->GetXaxis()->SetTitle("Truth-Reco cutFlow for EM jets");
    h_jet_T_EM_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_jet_T_EM_cutFlow->GetXaxis()->SetBinLabel(2,"Matching");
    h_jet_T_EM_cutFlow->GetXaxis()->SetBinLabel(3,"Reco iso");
    h_jet_T_EM_cutFlow->GetXaxis()->SetBinLabel(4,"Truth iso");

    h_jet_T_LC_cutFlow->GetXaxis()->SetTitle("Truth-Reco cutFlow for LC jets");
    h_jet_T_LC_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_jet_T_LC_cutFlow->GetXaxis()->SetBinLabel(2,"Matching");
    h_jet_T_LC_cutFlow->GetXaxis()->SetBinLabel(3,"Reco iso");
    h_jet_T_LC_cutFlow->GetXaxis()->SetBinLabel(4,"Truth iso");

    h_10jet_T_cutFlow->GetXaxis()->SetTitle("Truth-Reco cutFlow for 1.0 jets");
    h_10jet_T_cutFlow->GetXaxis()->SetBinLabel(1,"No cut");
    h_10jet_T_cutFlow->GetXaxis()->SetBinLabel(2,"Matching");
    h_10jet_T_cutFlow->GetXaxis()->SetBinLabel(3,"Reco iso");
    h_10jet_T_cutFlow->GetXaxis()->SetBinLabel(4,"Truth iso");

    wk()->addOutput (h_events);
    wk()->addOutput (h_ev_LC_cutFlow);
    wk()->addOutput (h_ev_EM_cutFlow);
    wk()->addOutput (h_ev_10_cutFlow);
    wk()->addOutput (h_jet_LC_cutFlow);
    wk()->addOutput (h_jet_EM_cutFlow);
    wk()->addOutput (h_10jet_cutFlow);
    wk()->addOutput (h_jet_T_EM_cutFlow);
    wk()->addOutput (h_jet_T_LC_cutFlow);
    wk()->addOutput (h_10jet_T_cutFlow);
    wk()->addOutput (h_dr_10Matching);
    wk()->addOutput (h_dr_2d_10Matching);
    wk()->addOutput (h_matching_ineff_count);
    wk()->addOutput (h_totalTjets_count);


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: changeInput (bool firstFile)
{
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.




    //  TEnv *m_settings = new TEnv();

    //  int status=m_settings->ReadFile(m_string.Data(),EEnvLevel(0));
    //  if (status!=0) error(Form("Cannot read file %s",m_string.Data()));

    Info("configure()", "User configuration read from : %s \n", m_config.c_str());

    m_config = gSystem->ExpandPathName( m_config.c_str() );
    TEnv* m_settings = new TEnv(m_config.c_str());
    if( !m_settings ) {
        Error("xAODAnalysis::initiaize()", "Failed to read config file!");
        Error("xAODAnalysis::initiaize()", "config name : %s",m_config.c_str());
    }


    t = clock();

    m_eventCounter = 0;

    m_event = wk()->xaodEvent();


    m_jetAlgos = Vectorize(m_settings->GetValue("JetAlgos",""));
    m_TruthjetAlgos = Vectorize(m_settings->GetValue("TruthJetAlgos",""));


    for (int algo=0;algo<m_jetAlgos.size();++algo) {
        Str jetAlgo  = m_jetAlgos[algo];

        Str treeName=jetAlgo;
        TTree* m_t = new TTree(treeName,"Tree for NI calibration");
        m_trees[treeName] = m_t;

        wk()->addOutput (m_trees[treeName]);


        m_t->Branch("EventWeight",&m_EVTvalues["EventWeight"],"EventWeight/D");
        m_t->Branch("E_corr1","vector<double>",&m_values["E_corr1"]);
        m_t->Branch("E_corr2","vector<double>",&m_values["E_corr2"]);
        m_t->Branch("m_corr1","vector<double>",&m_values["m_corr1"]);
        m_t->Branch("m_corr2","vector<double>",&m_values["m_corr2"]);
        m_t->Branch("E_calo","vector<double>",&m_values["E_calo"]);
        m_t->Branch("m_calo","vector<double>",&m_values["m_calo"]);
        m_t->Branch("E_true","vector<double>",&m_values["E_true"]);
        m_t->Branch("eta_corr1","vector<double>",&m_values["eta_corr1"]);
        m_t->Branch("eta_corr2","vector<double>",&m_values["eta_corr2"]);
        m_t->Branch("eta_calo","vector<double>",&m_values["eta_calo"]);
        m_t->Branch("phi_calo","vector<double>",&m_values["phi_calo"]);
        m_t->Branch("eta_true","vector<double>",&m_values["eta_true"]);
        m_t->Branch("phi_corr1","vector<double>",&m_values["phi_corr1"]);
        m_t->Branch("phi_corr2","vector<double>",&m_values["phi_corr2"]);
        m_t->Branch("phi_true","vector<double>",&m_values["phi_true"]);
        m_t->Branch("m_true","vector<double>",&m_values["m_true"]);
        m_t->Branch("index","vector<int>",&m_Intvalues["index"]);
        m_t->Branch("mu",&m_EVTvalues["mu"],"mu/D");
        m_t->Branch("NPV",&m_EVTvalues["NPV"],"NPV/D");
    }


    // variable initalization  
    m_matchingCut  =  m_settings->GetValue("MatchingCut",-99.0);
    m_recoIsoDR    =  m_settings->GetValue("RecoIsolationDR",-99.0);
    m_recoIsoPtCut =  m_settings->GetValue("RecoIsolationPtCut",-99.0);
    m_trueIsoDR    =  m_settings->GetValue("TruthIsolationDR",-99.0);
    m_trueIsoPtCut =  m_settings->GetValue("TruthIsolationPtCut",-99.0);
    m_pTRatioMin   =  m_settings->GetValue("MinPtRatioCut",0); 
    m_pTRatioMax   =  m_settings->GetValue("MaxPtRatioCut",1.4);

    m_weightFlag = m_settings->GetValue("WeightFlag",true);

    printf("\nSELECTION:\n==============\n");
    printf("\n  Truth-Reco matching cut: DR < %.2f\n",m_matchingCut);
    if (m_recoIsoDR>0) {
        printf("\n  Reco jets required to be isolated:\n  No other reco jets with pT > %.2f GeV within DR of %.2f\n",
                m_recoIsoPtCut, m_recoIsoDR);
    } else printf("\n  No reco jet isolation required\n");
    if (m_trueIsoDR>0) {
        printf("\n  Truth jets required to be isolated:\n  No other truth jets with pT > %.2f GeV within DR of %.2f\n",
                m_trueIsoPtCut, m_trueIsoDR);
    } else printf("\n  No truth jet isolation required\n");

    printf("\n==============\n");


    m_ApplyJES =  m_settings->GetValue("ApplyJES", false);


    // Initialize JetCalibTool

    const std::string name_to_JetCalibTools_LC = "LC"; //string describing the current thread, for logging
    const std::string name_to_JetCalibTools_EM = "EM"; //string describing the current thread, for logging
    TString calibSeq_LC_1 = "JetArea_Residual_Origin"; //String describing the calibration sequence to apply (see below)
    TString calibSeq_LC_2 = "JetArea_Residual_Origin_EtaJES"; //String describing the calibration sequence to apply (see below)
    TString calibSeq_EM_1 = "JetArea_Residual_Origin"; //String describing the calibration sequence to apply (see below)
    TString calibSeq_EM_2 = "JetArea_Residual_Origin_EtaJES"; //String describing the calibration sequence to apply (see below)


    myJES_akt4LC_1 = new JetCalibrationTool(name_to_JetCalibTools_LC,"AntiKt4LCTopo", JES_config_file, calibSeq_LC_1, m_isCollisions);
    myJES_akt4LC_2 = new JetCalibrationTool(name_to_JetCalibTools_LC,"AntiKt4LCTopo", JES_config_file, calibSeq_LC_2, m_isCollisions);
    myJES_akt4EM_1 = new JetCalibrationTool(name_to_JetCalibTools_EM,"AntiKt4EMTopo", JES_config_file, calibSeq_EM_1, m_isCollisions);
    myJES_akt4EM_2 = new JetCalibrationTool(name_to_JetCalibTools_EM,"AntiKt4EMTopo", JES_config_file, calibSeq_EM_2, m_isCollisions);


    myJES_akt4LC_1->initializeTool(name_to_JetCalibTools_LC);
    myJES_akt4LC_2->initializeTool(name_to_JetCalibTools_LC);
    myJES_akt4EM_1->initializeTool(name_to_JetCalibTools_EM);
    myJES_akt4EM_2->initializeTool(name_to_JetCalibTools_EM);


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.



    if( (m_eventCounter % 10) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;

    h_events->Fill(0);

    const xAOD::EventInfo* eventInfo = 0;
    if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
        Error("execute()", "Failed to retrieve event info collection. Exiting." );
        return EL::StatusCode::FAILURE;
    }


    double ev_weight=0;

    ev_weight = eventInfo->mcEventWeight();

    ev_ControlCut("",0);

    /* Vertex PU cut not used anymore (now replaced by m_weightFlag=True cut) 
     
    ///// Vertex PU cut ////////////////////////////////////////////////////////////////////////////////////

    const xAOD::VertexContainer* RecovertexContainer = 0;
    if( ! m_event->retrieve( RecovertexContainer, "PrimaryVertices").isSuccess() ){
        Error("execute()", "Failed to retrieve primary vertices collection. Exiting." );
        return EL::StatusCode::FAILURE;
    }
    auto zRecoVertex = RecovertexContainer->at(0)->z();

    xAOD::VertexContainer::const_iterator vtx_itr = RecovertexContainer->begin();
    xAOD::VertexContainer::const_iterator vtx_end = RecovertexContainer->end();

    
    //   int index(0);
    //   int reco_index(0);
    //   for( ; vtx_itr != vtx_end; ++vtx_itr) {

    //   if ((*vtx_itr)->vertexType()==1) reco_index=index;

    //   index++;
    //   }

    //   cout<<"reco index: "<<reco_index<<" "<<RecovertexContainer->at(reco_index)->vertexType()<<" "<<RecovertexContainer->at(0)->vertexType()<<endl;
    // 

    // Truth Vertex
    const xAOD::TruthVertexContainer* truthVertex = 0;
    if( ! m_event->retrieve( truthVertex, "TruthVertices").isSuccess() ){
        Error("execute()", "Failed to retrieve Truth Vertex container. Exiting." );
        return EL::StatusCode::FAILURE;
    }


    xAOD::TruthVertexContainer::const_iterator Tvtx_itr = truthVertex->begin();
    xAOD::TruthVertexContainer::const_iterator Tvtx_end = truthVertex->end();

    int Tindex(0);
    int truth_index(0);
    int barcode(-999);

    for( ; Tvtx_itr != Tvtx_end; ++Tvtx_itr) {


        if ((*Tvtx_itr)->barcode() < 0 && (*Tvtx_itr)->barcode()>barcode) {

            barcode = (*Tvtx_itr)->barcode();
            truth_index=Tindex;
        }
        Tindex++;
    }


    auto zTruthVertex = truthVertex->at(truth_index)->z();


    double deltaPVz = zRecoVertex-zTruthVertex;



    if(fabs(deltaPVz)>0.2) return EL::StatusCode::SUCCESS; // Skip event!

    ev_ControlCut("",1);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    */

    ev_ControlCut("",1);

    //Loop over jet collections
    for (int algo=0;algo<m_jetAlgos.size();++algo) {

        Str jetAlgo    = m_jetAlgos[algo];
        Str truthAlgo  = m_TruthjetAlgos[algo];

        std::cout<<"JetAlgo: "<<jetAlgo<<"\t"<<truthAlgo<<std::endl;

        // get reco jet container
        const xAOD::JetContainer* jets = 0;


        if ( !m_event->retrieve( jets, (string)jetAlgo).isSuccess() ){ 
            Error("execute()", "Failed to retrieve JetAlgo container. Exiting.");

            return EL::StatusCode::FAILURE;
        }		


        xAOD::JetContainer::const_iterator jet_itr = jets->begin();
        xAOD::JetContainer::const_iterator jet_end = jets->end();


        // get truth jet container
        const xAOD::JetContainer* truth_jets = 0;

        if ( !m_event->retrieve( truth_jets, (string)truthAlgo).isSuccess() ){ 
            Error("execute()", "Failed to retrieve TruthAlgo container. Exiting.");

            return EL::StatusCode::FAILURE;
        }


        xAOD::JetContainer::const_iterator truth_jet_itr = truth_jets->begin();
        xAOD::JetContainer::const_iterator truth_jet_end = truth_jets->end();

        // Temporary variables definition

        JetV recoJetsCorr2;
        JetV recoJetsCorr1;
        JetV recoJets_calo;
        JetV truthJets;

        vector<double> E_corr1;
        vector<double> m_corr1;
        vector<double> eta_corr1;
        vector<double> phi_corr1;

        vector<double> E_corr2;
        vector<double> m_corr2;
        vector<double> eta_corr2;
        vector<double> phi_corr2;

        vector<double> E_calo;
        vector<double> m_calo;
        vector<double> eta_calo;
        vector<double> phi_calo;

        vector<double> E_true;
        vector<double> m_true;
        vector<double> eta_true;
        vector<double> phi_true;

        vector<int> Vindex;


        // get mu
        float mu= eventInfo->averageInteractionsPerCrossing();
        //get NPV
        double NPV_corrected(0.0);


        // get vertex container
        const xAOD::VertexContainer* vertexContainer = 0;

        if( ! m_event->retrieve( vertexContainer, "PrimaryVertices").isSuccess() ){
            Error("execute()", "Failed to retrieve primary vertices collection. Exiting." );
            return EL::StatusCode::FAILURE;
        }

        xAOD::VertexContainer::const_iterator vtx_itr = vertexContainer->begin();
        xAOD::VertexContainer::const_iterator vtx_end = vertexContainer->end();

        int NPV=0;

        for( ; vtx_itr != vtx_end; ++vtx_itr ) {

            if ((*vtx_itr)->nTrackParticles()>=2) 
                NPV++;
        }

        NPV_corrected = (double) NPV;

        //loop in jets

        for( ; jet_itr != jet_end; ++jet_itr ) {

            jet_ControlCut(jetAlgo,0);

            double rawE = (*jet_itr)->e();
            double rawM = (*jet_itr)->m();

            if (rawM > rawE) continue; //skip unphysical jet

            jet_ControlCut(jetAlgo,1);

            // Apply cleanning tool
            //			if( !m_jetCleaning->accept( **jet_itr )) {
            //				continue; //only keep good clean jets
            //			}

            jet_ControlCut(jetAlgo,2);

            //get reco values from a given scale

            double pt=0;
            double eta=0;
            double phi=0;
            double e=0;


            pt = (*jet_itr)->jetP4(xAOD::JetConstitScaleMomentum).Pt()/1000;
            eta = (*jet_itr)->jetP4(xAOD::JetConstitScaleMomentum).Eta();
            phi = (*jet_itr)->jetP4(xAOD::JetConstitScaleMomentum).Phi();
            e = (*jet_itr)->jetP4(xAOD::JetConstitScaleMomentum).E()/1000;


            TLorentzVector jet_calo;
            jet_calo.SetPtEtaPhiE(pt,eta,phi,e);


            TLorentzVector jet_corr1;
            TLorentzVector jet_corr2;


            if (m_ApplyJES !=0 && jetAlgo.Contains("AntiKt4LCTopoJets")) {

                xAOD::Jet * jet_1 = 0;
                myJES_akt4LC_1->calibratedCopy(**jet_itr,jet_1); //make a calibrated copy
                jet_corr1.SetPtEtaPhiE(jet_1->pt()*0.001, jet_1->eta(), jet_1->phi(), jet_1->e()*0.001);
                delete jet_1;

                xAOD::Jet * jet_2 = 0;
                myJES_akt4LC_2->calibratedCopy(**jet_itr,jet_2); //make a calibrated copy
                jet_corr2.SetPtEtaPhiE(jet_2->pt()*0.001, jet_2->eta(), jet_2->phi(), jet_2->e()*0.001);
                delete jet_2;

            }

            else if (m_ApplyJES !=0 && jetAlgo.Contains("AntiKt4EMTopoJets")) {

                xAOD::Jet * jet_1 = 0;
                myJES_akt4EM_1->calibratedCopy(**jet_itr,jet_1); //make a calibrated copy
                jet_corr1.SetPtEtaPhiE(jet_1->pt()*0.001, jet_1->eta(), jet_1->phi(), jet_1->e()*0.001);
                delete jet_1;

                xAOD::Jet * jet_2 = 0;
                myJES_akt4EM_2->calibratedCopy(**jet_itr,jet_2); //make a calibrated copy
                jet_corr2.SetPtEtaPhiE(jet_2->pt()*0.001, jet_2->eta(), jet_2->phi(), jet_2->e()*0.001);
                delete jet_2;


            }


            else {
                jet_corr1.SetPtEtaPhiE(pt,eta,phi,e);
                jet_corr2.SetPtEtaPhiE(pt,eta,phi,e);
            }

            recoJetsCorr2.push_back(jet_corr2);
            recoJetsCorr1.push_back(jet_corr1);
            recoJets_calo.push_back(jet_calo);

        }// end for loop over jets


        //loop in TruthJets

        for( ; truth_jet_itr != truth_jet_end; ++truth_jet_itr ) {


            double pt = (*truth_jet_itr)->pt()/1000;
            double eta = (*truth_jet_itr)->eta();
            double phi = (*truth_jet_itr)->phi();
            double e = (*truth_jet_itr)->e()/1000;

            if (pt<7) continue; //FIXME

            TLorentzVector jet;
            jet.SetPtEtaPhiE(pt,eta,phi,e);

            truthJets.push_back(jet);

        }// end for loop over jets

        int NtruthJets = truthJets.size(); 
        int NrecoJets = recoJetsCorr1.size(); 


        if (m_weightFlag){
            double avgRecoPt; 

            if ((NrecoJets!=0 && NtruthJets!=0)){
                /*
                   double sum = 0;  
                   for (int a=0; a<NrecoJets; a++){ 
                   sum += recoJetsCorr1[a].Pt();
                   }
                   avgRecoPt = sum/NrecoJets;
                   */

                double leadRecoPt = recoJetsCorr1[0].Pt(); 
                int ind=0;

                //Get leading reco jet
                for (int b=1; b<NrecoJets; b++) {
                    if (recoJetsCorr1[b].Pt()>leadRecoPt){
                        leadRecoPt = recoJetsCorr1[b].Pt();
                        ind=b;
                    }
                }

                //Get subleading reco jet
                double subleadRecoPt = recoJetsCorr1[1].Pt();
                for (int b=1; b<NrecoJets; b++) {
                    if ((b != ind) && recoJetsCorr1[b].Pt()>subleadRecoPt){
                        subleadRecoPt = recoJetsCorr1[b].Pt();
                    }
                }

                //Get leading truth jet
                double leadTruthPt = truthJets[0].Pt(); 
                for (int b=1; b<NtruthJets; b++) {
                    if (truthJets[b].Pt()>leadTruthPt){
                        leadTruthPt = truthJets[b].Pt();
                    } 
                }

                avgRecoPt = (leadRecoPt+subleadRecoPt)/2;

                //Skip events affected by pile-up jets

                double qualityPtRatio = avgRecoPt/leadTruthPt; 
                if ((qualityPtRatio<m_pTRatioMin) || (qualityPtRatio>m_pTRatioMax)){
                    //  cout << "Skipping event which fails Pt ratio cut" << endl;
                    //  cout << "avgRecoPt: " << avgRecoPt << " and leadTruthPt: " << leadTruthPt << endl;  
                    //  cout << "weight: " << w << " and ratio: " << qualityPtRatio << endl; 
                    return EL::StatusCode::SUCCESS; //skip event 
                }
            }
        }

        ev_ControlCut(jetAlgo,2);

        for (int ti=0;ti<NtruthJets;++ti) {
            Jet truthJet = truthJets[ti];

            jet_T_ControlCut(jetAlgo, 0);

            // Let's find the matching reco jet
            Jet recoJet; 
            int index(0);
            int Nmatches = TruthMatch_index(truthJets[ti],recoJetsCorr1,recoJet,index);

            // skip truth jets that don't match any reco jets
            if (Nmatches==0) continue; 

            jet_T_ControlCut(jetAlgo, 1);

            if ( m_recoIsoDR > 0 ) {
                double DRminReco = DRmin(recoJet,recoJetsCorr1,m_recoIsoPtCut);
                if ( DRminReco < m_recoIsoDR ) continue;
            }

            jet_T_ControlCut(jetAlgo, 2);

            if ( m_trueIsoDR > 0 ) {
                double DRminTruth = DRmin(truthJet,truthJets,m_trueIsoPtCut);
                if ( DRminTruth < m_trueIsoDR ) continue;
            }

            jet_T_ControlCut(jetAlgo, 3);

            //Storing variables
            E_true.push_back(truthJet.E());
            eta_true.push_back(truthJet.Eta());
            phi_true.push_back(truthJet.Phi());
            m_true.push_back(truthJet.M());

            E_calo.push_back(recoJets_calo[index].E());
            m_calo.push_back(recoJets_calo[index].M());
            eta_calo.push_back(recoJets_calo[index].Eta());
            phi_calo.push_back(recoJets_calo[index].Phi());

            E_corr1.push_back(recoJetsCorr1[index].E());
            m_corr1.push_back(recoJetsCorr1[index].M());
            eta_corr1.push_back(recoJetsCorr1[index].Eta());
            phi_corr1.push_back(recoJetsCorr1[index].Phi());

            E_corr2.push_back(recoJetsCorr2[index].E());
            m_corr2.push_back(recoJetsCorr2[index].M());
            eta_corr2.push_back(recoJetsCorr2[index].Eta());
            phi_corr2.push_back(recoJetsCorr2[index].Phi());

            Vindex.push_back(index);
        } // end loop in jets


        Str treeName=jetAlgo;

        TTree *t = m_trees[treeName];

        if (t==NULL) error("Can't find tree "+treeName+" for jet algo ");


        m_EVTvalues["EventWeight"]=ev_weight;

        m_values["E_true"]=E_true;
        m_values["eta_true"]=eta_true;
        m_values["phi_true"]=phi_true;
        m_values["m_true"]=m_true;

        m_values["E_calo"]=E_calo;
        m_values["m_calo"]=m_calo;
        m_values["eta_calo"]=eta_calo;
        m_values["phi_calo"]=phi_calo;

        m_values["E_corr1"]=E_corr1;
        m_values["eta_corr1"]=eta_corr1;
        m_values["phi_corr1"]=phi_corr1;
        m_values["m_corr1"]=m_corr1;

        m_values["E_corr2"]=E_corr2;
        m_values["eta_corr2"]=eta_corr2;
        m_values["phi_corr2"]=phi_corr2;
        m_values["m_corr2"]=m_corr2;

        m_Intvalues["index"]=Vindex;
        m_EVTvalues["mu"]=mu;
        m_EVTvalues["NPV"]=NPV_corrected;


        t->Fill();


    }//for each jet algo

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.



    if( myJES_akt4LC_1 ) {
        delete myJES_akt4LC_1;
        myJES_akt4LC_1 = 0;
    }

    if( myJES_akt4LC_2 ) {
        delete myJES_akt4LC_2;
        myJES_akt4LC_2 = 0;
    }


    if( myJES_akt4EM_1 ) {
        delete myJES_akt4EM_1;
        myJES_akt4EM_1 = 0;
    }

    if( myJES_akt4EM_2 ) {
        delete myJES_akt4EM_2;
        myJES_akt4EM_2 = 0;
    }

    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODAnalysis :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    return EL::StatusCode::SUCCESS;
}


void xAODAnalysis :: ev_ControlCut(TString jetAlgo, int i) {

    if (jetAlgo.Contains("AntiKt4LCTopoJets")) ev_LC_cutFlow(i);
    if (jetAlgo.Contains("AntiKt4EMTopoJets")) ev_EM_cutFlow(i);
    if (jetAlgo.Contains("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets")) ev_10_cutFlow(i);
    if (jetAlgo.IsNull()) {
        ev_EM_cutFlow(i);
        ev_LC_cutFlow(i);
        ev_10_cutFlow(i);
    }
}


void xAODAnalysis :: jet_ControlCut(TString jetAlgo, int i) {

    if (jetAlgo.Contains("AntiKt4LCTopoJets")) jet_LC_cutFlow(i);
    if (jetAlgo.Contains("AntiKt4EMTopoJets")) jet_EM_cutFlow(i);
    if (jetAlgo.Contains("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets")) Fatjet_cutFlow(i);

}

void xAODAnalysis :: jet_T_ControlCut(TString jetAlgo, int i) {

    if (jetAlgo.Contains("AntiKt4LCTopoJets"))  jet_Truth_LC_cutFlow(i);
    if (jetAlgo.Contains("AntiKt4EMTopoJets"))  jet_Truth_EM_cutFlow(i);
    if (jetAlgo.Contains("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"))  Fatjet_Truth_cutFlow(i);

}

void xAODAnalysis::ev_LC_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_ev_LC_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_ev_LC_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_ev_LC_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_ev_LC_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_ev_LC_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_LC_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_LC_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::ev_EM_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_ev_EM_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_ev_EM_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_ev_EM_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_ev_EM_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_ev_EM_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_EM_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_EM_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::ev_10_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_ev_10_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_ev_10_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_ev_10_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_ev_10_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_ev_10_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_10_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_ev_10_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::jet_LC_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_jet_LC_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_jet_LC_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_jet_LC_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_jet_LC_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_jet_LC_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_LC_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_LC_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::jet_EM_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_jet_EM_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_jet_EM_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_jet_EM_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_jet_EM_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_jet_EM_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_EM_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_EM_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::Fatjet_cutFlow (int i) {

    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_10jet_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_10jet_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_10jet_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_10jet_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_10jet_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_cutFlow->Fill(6);
            break;
    }
}

void xAODAnalysis::jet_Truth_EM_cutFlow (int i) {


    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_jet_T_EM_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_jet_T_EM_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_jet_T_EM_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_jet_T_EM_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_jet_T_EM_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_EM_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_EM_cutFlow->Fill(6);
            break;

        case 7: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_EM_cutFlow->Fill(7);
            break;

        case 8: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_EM_cutFlow->Fill(8);
            break;


    }
}


void xAODAnalysis::jet_Truth_LC_cutFlow (int i) {


    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_jet_T_LC_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_jet_T_LC_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_jet_T_LC_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_jet_T_LC_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_jet_T_LC_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_LC_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_LC_cutFlow->Fill(6);
            break;

        case 7: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_LC_cutFlow->Fill(7);
            break;

        case 8: 
            //	ATH_MSG_INFO("passing cut 5");
            h_jet_T_LC_cutFlow->Fill(8);
            break;

    }
}

void xAODAnalysis::Fatjet_Truth_cutFlow (int i) {


    switch (i) {
        case 0:
            //	ATH_MSG_INFO("passing cut 0");
            h_10jet_T_cutFlow->Fill(0);
            break;
        case 1:
            //	ATH_MSG_INFO("passing cut 1");
            h_10jet_T_cutFlow->Fill(1);
            break;
        case 2: 
            //	ATH_MSG_INFO("passing cut 2");
            h_10jet_T_cutFlow->Fill(2);
            break;
        case 3: 
            //	ATH_MSG_INFO("passing cut 3");
            h_10jet_T_cutFlow->Fill(3);
            break;
        case 4: 
            //	ATH_MSG_INFO("passing cut 4");
            h_10jet_T_cutFlow->Fill(4);
            break;
        case 5: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_T_cutFlow->Fill(5);
            break;

        case 6: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_T_cutFlow->Fill(6);
            break;

        case 7: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_T_cutFlow->Fill(7);
            break;

        case 8: 
            //	ATH_MSG_INFO("passing cut 5");
            h_10jet_T_cutFlow->Fill(8);
            break;

    }
}

int xAODAnalysis :: TruthMatch(Jet myjet, JetV jets, Jet &matchingJet) {
    int Nmatches=0;
    for (uint ji=0;ji<jets.size();++ji)
        if (myjet.DeltaR(jets[ji])<m_matchingCut) {
            if (Nmatches==0) matchingJet=jets[ji];
            ++Nmatches;
        }
    return Nmatches;
}

int xAODAnalysis :: TruthMatch_index(Jet myjet, JetV jets, Jet &matchingJet, int &index) {
    int Nmatches=0;

    double drmin = 999;
    int Min_index=-1;

    h_totalTjets_count->Fill(0);

    for (uint ji=0;ji<jets.size();++ji) {

        double dr = myjet.DeltaR(jets[ji]);

        if (dr < m_matchingCut) ++Nmatches;

        //find minimum:
        if (dr < drmin) {
            drmin = dr;
            Min_index = ji;
        }
    }

    if (jets.size()==0) {
        h_matching_ineff_count->Fill(0);
    }

    if (drmin<m_matchingCut) {
        matchingJet=jets[Min_index]; index=Min_index;
    }

    h_dr_10Matching->Fill(drmin);
    h_dr_2d_10Matching->Fill(myjet.Pt(),drmin);

    return Nmatches;
}


double xAODAnalysis :: DRmin(Jet myjet, JetV jets, double PtMin) {
    double DRmin=9999;
    for (uint ji=0;ji<jets.size();++ji) {
        if (PtMin>0&&jets[ji].Pt()<PtMin) continue;
        double DR=myjet.DeltaR(jets[ji]);
        if ( DR>0.0001 && DR<DRmin ) DRmin=DR;
    }
    return DRmin;
}

StrV xAODAnalysis :: Vectorize(Str str) {
    StrV result;
    TObjArray *strings = str.Tokenize(" ");
    if (strings->GetEntries()==0) return result;
    TIter istr(strings);
    while (TObjString* os=(TObjString*)istr())
        add(result,os->GetString());

    return result;
}



