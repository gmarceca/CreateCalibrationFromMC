#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleMeta.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/GridDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "TString.h"
#include "CreateCalibrationTreeFromxAOD/xAODAnalysis.h"
#include <sstream>

int main( int argc, char* argv[] ) {


    std::string m_configName = "$ROOTCOREBIN/data/CreateCalibrationTreeFromxAOD/AntiKt4Topo.config";
    std::string m_configuration = "$ROOTCOREBIN/data/CreateCalibrationTreeFromxAOD/AntiKt4Topo.config";


    Info("configure()", "User configuration read from : %s \n", m_configName.c_str());

    m_configName = gSystem->ExpandPathName( m_configName.c_str() );
    TEnv* settings = new TEnv(m_configName.c_str());
    if( !settings ) {
        Error("xAODrun", "Failed to read config file!");
        Error("xAODrun", "config name : %s",m_configName.c_str());
    }


    std::string input = settings->GetValue("Input","/afs/cern.ch/user/g/gmarceca/WORK/sample");
    std::string submitDir = settings->GetValue("Output","submitDir");
    std::string directory; //Sample directory
    std::string USER_output_path; //Sample directory
    std::string grid; //Sample directory

    // Construct the samples to run on: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler
    // this object manage multiples samples in a directory
    SH::SampleHandler sh;  


    for (int i=1; i< argc; i++){


        std::string opt(argv[i]); std::vector< std::string > v;

        std::istringstream iss(opt);

        std::string item;
        char delim = '=';

        while (std::getline(iss, item, delim)){
            v.push_back(item);
        }

        // Take the submit directory from the input:
        if ( opt.find("--submitDir=")   != std::string::npos) submitDir = v[1];

        if ( opt.find("--sample=")   != std::string::npos) directory = v[1];

        if ( opt.find("--output=")   != std::string::npos) USER_output_path = v[1];

        if ( opt.find("--grid=")   != std::string::npos) grid = v[1];

    }//End: Loop over input options

    std::cout<<"USER_output_path: "<<USER_output_path<<std::endl;
    std::cout<<"submitDir: "<<submitDir<<std::endl;
    std::cout<<"directory: "<<directory<<std::endl;
    std::cout<<"grid: "<<grid<<std::endl;



    if (grid.data()=="true") {

        SH::scanDQ2 (sh, directory); 

        sh.setMetaString( "nc_tree", "CollectionTree" );

        // Set up the job for xAOD access:
        xAOD::Init().ignore();

        // Create an EventLoop job:
        EL::Job job;
        job.sampleHandler(sh);

        // Add our analysis to the job:
        xAODAnalysis* alg = new xAODAnalysis(m_configuration);    
        job.algsAdd( alg );

        // To automatically delete submitDir
        job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

        EL::PrunDriver driver;

        if (!USER_output_path.data()) Error("xAODrun", "Set USER_output_path!");

        driver.options()->setString("nc_outputSampleName", USER_output_path);
        // driver.options()->setDouble("nc_disableAutoRetry", 1); //Disabling AutoRetry

        driver.options()->setDouble("nc_nFilesPerJob", 1);
        //  driver.options()->setString("nc_rootVer", "5.34.22");
        driver.options()->setString("nc_cmtConfig", "x86_64-slc6-gcc48-opt");


        driver.submitOnly( job, submitDir );

    }

    else {

        std::cout<<"input: "<<input<<std::endl;

        SH::scanDir( sh, input);


        //    SH::DiskListLocal list (input);
        //  SH::scanDir (sh, list, "DAOD_JETM1.05130134._000017.pool.root.1");

        //SH::DiskListEOS list ("/eos/atlas/user/k/krumnack", "root://eosatlas//eos/atlas/user/k/krumnack"); or
        //SH::DiskListEOS list ("eosatlas.cern.ch", "/eos/atlas/user/k/krumnack");
        //SH::scanDir (sh, list);


        // Set the name of the input TTree. It's always "CollectionTree"
        // for xAOD files.
        sh.setMetaString( "nc_tree", "CollectionTree" );


        //
        // Set up the job for xAOD access:
        xAOD::Init().ignore();

        // Create an EventLoop job:
        EL::Job job;
        job.sampleHandler(sh);

        // Add our analysis to the job:
        xAODAnalysis* alg = new xAODAnalysis(m_configName);    
        job.algsAdd( alg );

        // To automatically delete submitDir
        job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);


        // Run the job using the local/direct driver:
        EL::DirectDriver driver;

        driver.submit( job, submitDir );

    }

    return 0;
}
